import http from "./httpServices";
import * as config from "../config.json";

const { Api } = config;
const perspectiveUrl = Api.perspective.baseUrl + "Perspectives";

export function getPerspectives() {
  return http.get(perspectiveUrl);
}

export function submitPerspective(userPerspective) {
  return http.post(perspectiveUrl, userPerspective);
}

export function getPerspectivesByEmailAddress(emailAddress) {
  return http.get(perspectiveUrl);
}
