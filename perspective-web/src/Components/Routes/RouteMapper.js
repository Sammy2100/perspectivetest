import { BrowserRouter, Route, Routes } from "react-router-dom";

import Home from "../Pages/Layout/Home";
import Result from "../Pages/Perspective/Result";
import NotFound from "../Pages/NotFound";

const RouteMapper = () => {
  return (
    <BrowserRouter>
      <Routes>
        <Route index element={<Home />} />
        <Route path="result" element={<Result />} />
        <Route path="*" element={<NotFound />} />
      </Routes>
    </BrowserRouter>
  );
};

export default RouteMapper;
