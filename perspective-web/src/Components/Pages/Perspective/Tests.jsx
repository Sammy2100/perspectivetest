import { useEffect, useState } from "react";
import { Col, Row } from "reactstrap";
import { getPerspectives } from "../../../Services/perspectiveService";
import Loader from "../Shared/Loader";
import Perspective from "./Perspective";

const Tests = () => {
  const [perspective, setPerspective] = useState();
  const [loader, setLoader] = useState(true);

  useEffect(() => {
    (async () => {
      // Fetch Perspectives data from the Perspectives endpoint
      try {
        const result = await getPerspectives();
        loadPerspectives(result.data);
      } catch (ex) {
        const perspectiveDiv = (
          <div className="text-danger">
            Error loading Perspective data. Try again later.
          </div>
        );
        setPerspective(perspectiveDiv);
      }
    })();
  }, [setPerspective, setLoader]);

  const loadPerspectives = (perspectives) => {
    if (perspectives.length === 0) {
      const perspectiveDiv = <div>No Perspective data to load!</div>;
      setPerspective(perspectiveDiv);
      setLoader(false);
    } else {
      const perspectiveDiv = <Perspective perspectives={perspectives} />;
      setPerspective(perspectiveDiv);
      setLoader(false);
    }
  };

  return (
    <div>
      <Row>
        <Col>
          <h5 className="fw-bolder">Discover Your Perspective</h5>
        </Col>
      </Row>
      <Row>
        <Col>
          Complete the 7 min test and get a detailed report of your lenses to
          the world.
        </Col>
      </Row>
      {loader ? <Loader /> : perspective}
    </div>
  );
};

export default Tests;
