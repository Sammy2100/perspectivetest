import { useLocation } from "react-router-dom";
import { Col, Container, Row } from "reactstrap";

const Result = () => {
  const { state } = useLocation();

  return (
    <div>
      <Container>
        <Row className="perspective-result">
          <Col xs="6" className="border align-middle">
            <h3>Your Perspective</h3>
            <p className="fw-bolder">
              Your Perspective Type is {state.perspectiveType}
            </p>
          </Col>
          <Col xs="6" className="border align-middle">
            <Row>
              <Col xs="3">Introversion (I)</Col>
              <Col
                xs="3"
                className={
                  state.perspectiveType.includes("I")
                    ? "perspective-selected"
                    : ""
                }
              ></Col>
              <Col
                xs="3"
                className={
                  state.perspectiveType.includes("E")
                    ? "perspective-selected"
                    : ""
                }
              ></Col>
              <Col xs="3">Extroversion (E)</Col>
            </Row>
            <Row>
              <Col xs="3">Sensing (S)</Col>
              <Col
                xs="3"
                className={
                  state.perspectiveType.includes("S")
                    ? "perspective-selected"
                    : ""
                }
              ></Col>
              <Col
                xs="3"
                className={
                  state.perspectiveType.includes("N")
                    ? "perspective-selected"
                    : ""
                }
              ></Col>
              <Col xs="3">Intuition (N)</Col>
            </Row>
            <Row>
              <Col xs="3">Thinking (T)</Col>
              <Col
                xs="3"
                className={
                  state.perspectiveType.includes("T")
                    ? "perspective-selected"
                    : ""
                }
              ></Col>
              <Col
                xs="3"
                className={
                  state.perspectiveType.includes("F")
                    ? "perspective-selected"
                    : ""
                }
              ></Col>
              <Col xs="3">Feeling (F)</Col>
            </Row>
            <Row>
              <Col xs="3">Judging (J)</Col>
              <Col
                xs="3"
                className={
                  state.perspectiveType.includes("J")
                    ? "perspective-selected"
                    : ""
                }
              ></Col>
              <Col
                xs="3"
                className={
                  state.perspectiveType.includes("P")
                    ? "perspective-selected"
                    : ""
                }
              ></Col>
              <Col xs="3">Perceiving (P)</Col>
            </Row>
          </Col>
        </Row>
      </Container>
    </div>
  );
};

export default Result;
