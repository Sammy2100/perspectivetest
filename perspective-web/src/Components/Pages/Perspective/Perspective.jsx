import { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import { Button, Col, Input, Row } from "reactstrap";
import { Formik, Form } from "formik";
import * as Yup from "yup";
import TextField from "../Shared/Form/TextField";
import { submitPerspective } from "../../../Services/perspectiveService";

const Perspective = ({ perspectives }) => {
  const [selected, setSelected] = useState();
  const [buttonProperty, setButtonProperty] = useState({
    text: "Save & Continue",
    disabled: false,
  });
  let navigate = useNavigate();

  useEffect(() => {
    (() => {
      const initVal = [];
      perspectives.forEach((value) => {
        const questions = {
          questionId: value.id,
          dimension: value.dimension,
          direction: value.direction,
          meaning: value.meaning,
          selection: 0,
        };
        initVal.push(questions);
      });

      setSelected(initVal);
    })();
  }, [setSelected, perspectives]);

  const onSelectedOption = ({ target }) => {
    // updating the selected option
    const questions = [...selected];
    const question = questions.find((q) => q.questionId === target.name);
    question.selection = parseInt(target.value);
    const index = questions.findIndex((q) => q.questionId === question.id);
    questions[index] = question;
    setSelected(questions);
  };

  const initialValues = {
    email: "",
  };

  const yupValidation = Yup.object({
    email: Yup.string()
      .email("Email address is invalid!")
      .required("Email address is required!"),
  });

  return (
    <Formik
      initialValues={initialValues}
      validationSchema={yupValidation}
      onSubmit={async (values, { setSubmitting, resetForm }) => {
        // checking if the user answers all questions
        const userSelections = selected.filter((s) => s.selection > 0);
        if (selected.length > userSelections.length) {
          alert(
            `You have ${
              selected.length - userSelections.length
            } more question selections to go. Kindly review them and select accordingly.`
          );
          return;
        }

        try {
          setButtonProperty({ text: "Processing...", disabled: true });
          const userPerspective = {
            emailAddress: values.email,
            perspectives: selected,
          };
          const { data } = await submitPerspective(userPerspective);
          navigate("result", { state: data });
        } catch (err) {
          if (err.response && err.response.data) {
            alert(err.response.data);
          } else {
            alert("An exception occured!");
          }
          setButtonProperty({ text: "Save & Continue", disabled: false });
        }

        setSubmitting(true);
        //resetForm();
      }}
    >
      {(props) => (
        <Form>
          {perspectives.map((perspective) => (
            <Row className="perspective" key={perspective.id}>
              <Col className="border">
                <Row className="fw-bolder">
                  <Col>{perspective.question}</Col>
                </Row>
                <Row className="perspective-response">
                  <Col>
                    <span className="text-danger text-end align-middle">
                      Disagree
                    </span>
                    <Input
                      type="radio"
                      className="input-selection"
                      name={perspective.id}
                      value="1"
                      onChange={onSelectedOption}
                    />
                    <Input
                      type="radio"
                      className="input-selection"
                      name={perspective.id}
                      value="2"
                      onChange={onSelectedOption}
                    />
                    <Input
                      type="radio"
                      className="input-selection"
                      name={perspective.id}
                      value="3"
                      onChange={onSelectedOption}
                    />
                    <Input
                      type="radio"
                      className="input-selection"
                      name={perspective.id}
                      value="4"
                      onChange={onSelectedOption}
                    />
                    <Input
                      type="radio"
                      className="input-selection"
                      name={perspective.id}
                      value="5"
                      onChange={onSelectedOption}
                    />
                    <Input
                      type="radio"
                      className="input-selection"
                      name={perspective.id}
                      value="6"
                      onChange={onSelectedOption}
                    />
                    <Input
                      type="radio"
                      className="input-selection"
                      name={perspective.id}
                      value="7"
                      onChange={onSelectedOption}
                    />
                    <span className="text-success text-start align-middle">
                      Agree
                    </span>
                  </Col>
                </Row>
              </Col>
            </Row>
          ))}
          <Row>
            <Col>
              <TextField
                label="Your Address"
                placeholder="you@example.com"
                name="email"
                type="email"
              />
            </Col>
          </Row>

          <div className="text-center">
            <Button size="1" color="primary" disabled={buttonProperty.disabled}>
              {buttonProperty.text}
            </Button>
          </div>
          <Row></Row>
        </Form>
      )}
    </Formik>
  );
};

export default Perspective;
