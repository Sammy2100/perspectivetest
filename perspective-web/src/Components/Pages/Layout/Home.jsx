import { Container } from "reactstrap";
import Tests from "../Perspective/Tests";

const Home = () => {
  return (
    <Container>
      <Tests />
    </Container>
  );
};

export default Home;
