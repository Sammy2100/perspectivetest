import RouteMapper from "./Components/Routes/RouteMapper";

function App() {
  return (
    <div className="App">
      <div>
        <RouteMapper />
      </div>
    </div>
  );
}

export default App;
