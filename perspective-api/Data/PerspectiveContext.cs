using Microsoft.EntityFrameworkCore;
using Perspective.Api.Models;

namespace Perspective.Api.Data
{
    public class PerspectiveContext : DbContext
    {
        public PerspectiveContext(DbContextOptions<PerspectiveContext> options) : base(options) { }

        public virtual DbSet<Perspectives> Perspectives { get; set; }
        public virtual DbSet<UserPerspective> UserPerspectives { get; set; }

    }
}