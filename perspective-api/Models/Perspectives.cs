using System;

namespace Perspective.Api.Models
{
    public class Perspectives
    {
        public Guid Id { get; set; }
        public string Question { get; set; }
        public string Dimension { get; set; }
        public int Direction { get; set; }
        public string Meaning { get; set; }

    }

}