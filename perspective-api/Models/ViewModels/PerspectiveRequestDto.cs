using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Perspective.Api.Models.ViewModels
{
    public class PerspectiveRequestDto
    {
        [Required]
        public string EmailAddress { get; set; }

        public List<PerspectiveDto> Perspectives { get; set; }
    }

    public class PerspectiveDto
    {
        [Required]
        public Guid QuestionId { get; set; }

        [Required]
        public string Dimension { get; set; }

        [Required]
        public int Direction { get; set; }

        [Required]
        public string Meaning { get; set; }

        [Required]
        public int Selection { get; set; }
    }

}