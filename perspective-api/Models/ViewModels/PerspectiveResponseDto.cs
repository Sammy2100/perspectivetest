using System.ComponentModel.DataAnnotations;

namespace Perspective.Api.Models.ViewModels
{
    public class PerspectiveResponseDto
    {
        public int Introversion { get; set; }
        public int Extroversion { get; set; }
        public int Sensing { get; set; }
        public int Intuition { get; set; }
        public int Thinking { get; set; }
        public int Feeling { get; set; }
        public int Judging { get; set; }
        public int Perceiving { get; set; }
        public int EI { get; set; }
        public int SN { get; set; }
        public int TF { get; set; }
        public int JP { get; set; }
        public string PerspectiveType { get; set; }
    }

}