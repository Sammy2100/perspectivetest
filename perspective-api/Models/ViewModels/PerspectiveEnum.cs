namespace Perspective.Api.Models.ViewModels
{
    public static class PerspectiveEnum
    {
        public const int ConfidencePoint = 4;
        public const string Introversion = "I";
        public const string Extroversion = "E";
        public const string Sensing = "S";
        public const string Intuition = "N";
        public const string Thinking = "T";
        public const string Feeling = "F";
        public const string Judging = "J";
        public const string Perceiving = "P";
    }

}