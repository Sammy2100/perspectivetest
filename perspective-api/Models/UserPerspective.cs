using System;

namespace Perspective.Api.Models
{
    public class UserPerspective
    {
        public Guid Id { get; set; }
        public string EmailAddress { get; set; }
        public Guid QuestionId { get; set; }
        public int Selection { get; set; }
        public string Dimension { get; set; }
        public int Direction { get; set; }
        public string Meaning { get; set; }

    }

}