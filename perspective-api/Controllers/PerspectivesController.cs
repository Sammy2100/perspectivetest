using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Perspective.Api.Business;
using Perspective.Api.Models;
using Perspective.Api.Models.ViewModels;

namespace PerspectiveApi.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class PerspectivesController : ControllerBase
    {
        private readonly IPerspectiveBusiness _perspectiveBusiness;

        public PerspectivesController(IPerspectiveBusiness perspectiveBusiness)
        {
            _perspectiveBusiness = perspectiveBusiness;
        }

        [HttpGet]
        public async Task<IActionResult> GetPerspectives()
        {
            var perspectives = await _perspectiveBusiness.GetPerspectives();
            return Ok(perspectives);
        }

        [HttpGet("[action]/{emailAddress}")]
        public async Task<IActionResult> GetPerspectivesByEmailAddress(string emailAddress)
        {
            var result = await _perspectiveBusiness.GetPerspectiveByEmailAdress(emailAddress);
            return Ok(result);
        }

        [HttpPost]
        public async Task<IActionResult> SuubmitPerspective(PerspectiveRequestDto perspectiveRequests)
        {
            var result = await _perspectiveBusiness.SubmitPerspective(perspectiveRequests);
            if (result == null)
            {
                return BadRequest($"Email Address [{perspectiveRequests.EmailAddress}] already exist in our database");
            }

            return Ok(result);
        }

    }

}