using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Perspective.Api.Data;
using Perspective.Api.Models;
using Perspective.Api.Models.ViewModels;

namespace Perspective.Api.Business
{
    public interface IPerspectiveBusiness
    {
        Task<List<Perspectives>> GetPerspectives();
        Task<PerspectiveResponseDto> GetPerspectiveByEmailAdress(string emailAddress);
        Task<PerspectiveResponseDto> SubmitPerspective(PerspectiveRequestDto perspectiveRequest);
    }
    public class PerspectiveBusiness : IPerspectiveBusiness
    {
        private readonly PerspectiveContext _context;

        public PerspectiveBusiness(PerspectiveContext context)
        {
            _context = context;
        }

        List<Perspectives> Perspectives =>
            new List<Perspectives>
            {
                new Perspectives {
                    Question = "You find it takes effort to introduce yourself to other people.",
                    Dimension = "EI",
                    Direction = 1,
                    Meaning = "I"
                },
                new Perspectives {
                    Question = "You consider yourself more practical than creative.",
                    Dimension = "SN",
                    Direction = -1,
                    Meaning = "S"
                },
                new Perspectives {
                    Question = "Winning a debate matters less to you than making sure no one gets upset.",
                    Dimension = "TF",
                    Direction = 1,
                    Meaning = "F"
                },
                new Perspectives {
                    Question = "You get energized going to social events that involve many interactions.",
                    Dimension = "EI",
                    Direction = -1,
                    Meaning = "E"
                },
                new Perspectives {
                    Question = "You often spend time exploring unrealistic and impractical yet intriguing ideas.",
                    Dimension = "SN",
                    Direction = 1,
                    Meaning = "N"
                },
                new Perspectives {
                    Question = "Deadlines seem to you to be of relative rather than absolute importance.",
                    Dimension = "JP",
                    Direction = 1,
                    Meaning = "P"
                },
                new Perspectives {
                    Question = "Logic is usually more important than heart when it comes to making important decisions.",
                    Dimension = "TF",
                    Direction = -1,
                    Meaning = "T"
                },
                new Perspectives {
                    Question = "Your home and work environments are quite tidy.",
                    Dimension = "JP",
                    Direction = -1,
                    Meaning = "J"
                },
                new Perspectives {
                    Question = "You do not mind being at the center of attention.",
                    Dimension = "EI",
                    Direction = -1,
                    Meaning = "E"
                },
                new Perspectives {
                    Question = "Keeping your options open is more important than having a to-do list.",
                    Dimension = "JP",
                    Direction = 1,
                    Meaning = "P"
                }
            };

        private List<UserPerspective> UserPerspectives =>
            new List<UserPerspective>{
                    new UserPerspective {
                        Selection = 4,
                        Dimension = "EI",
                        Direction = 1,
                        Meaning = "I"
                    },
                    new UserPerspective {
                        Selection = 3,
                        Dimension = "SN",
                        Direction = -1,
                        Meaning = "S"
                    },
                    new UserPerspective {
                        Selection = 1,
                        Dimension = "TF",
                        Direction = 1,
                        Meaning = "F"
                    },
                    new UserPerspective {
                        Selection = 6,
                        Dimension = "EI",
                        Direction = -1,
                        Meaning = "E"
                    },
                    new UserPerspective {
                        Selection = 7,
                        Dimension = "SN",
                        Direction = 1,
                        Meaning = "N"
                    },
                    new UserPerspective {
                        Selection = 3,
                        Dimension = "JP",
                        Direction = 1,
                        Meaning = "P"
                    },
                    new UserPerspective {
                        Selection = 5,
                        Dimension = "TF",
                        Direction = -1,
                        Meaning = "T"
                    },
                    new UserPerspective {
                        Selection = 3,
                        Dimension = "JP",
                        Direction = -1,
                        Meaning = "J"
                    },
                    new UserPerspective {
                        Selection = 6,
                        Dimension = "EI",
                        Direction = -1,
                        Meaning = "E"
                    },
                    new UserPerspective {
                        Selection = 6,
                        Dimension = "JP",
                        Direction = 1,
                        Meaning = "P"
                    },
                };

        public async Task<List<Perspectives>> GetPerspectives()
        {
            //_context.Perspectives.AddRange(Perspectives);
            //await _context.SaveChangesAsync();
            var perspectives = await _context.Perspectives.ToListAsync();
            return perspectives;
        }

        public async Task<PerspectiveResponseDto> GetPerspectiveByEmailAdress(string emailAddress)
        {
            var userPerspectives = await _context.UserPerspectives
            .Where(u => u.EmailAddress == emailAddress)
            .ToListAsync();

            var perspective = ProcessUserPerspective(userPerspectives);
            return perspective;
        }

        public async Task<PerspectiveResponseDto> SubmitPerspective(PerspectiveRequestDto perspectiveRequest)
        {
            // check if email exists
            var email = _context.UserPerspectives.FirstOrDefault(u => u.EmailAddress == perspectiveRequest.EmailAddress);
            if (email != null)
            {
                // user already exist
                return null;
            }

            var userPerspectives = perspectiveRequest.Perspectives.Select(perspective => new UserPerspective
            {
                EmailAddress = perspectiveRequest.EmailAddress,
                QuestionId = perspective.QuestionId,
                Dimension = perspective.Dimension,
                Direction = perspective.Direction,
                Meaning = perspective.Meaning,
                Selection = perspective.Selection
            }).ToList();

            _context.UserPerspectives.AddRange(userPerspectives);
            await _context.SaveChangesAsync();

            var perspective = ProcessUserPerspective(userPerspectives);
            return perspective;
        }

        private PerspectiveResponseDto ProcessUserPerspective(List<UserPerspective> userPerspectives)
        {
            var result = new PerspectiveResponseDto();

            foreach (var perspective in userPerspectives)
            {
                switch (perspective.Meaning)
                {
                    case PerspectiveEnum.Introversion:
                        result.EI += perspective.Direction;
                        if (perspective.Selection <= PerspectiveEnum.ConfidencePoint)
                            result.Introversion++;
                        else
                            result.Extroversion++;
                        break;

                    case PerspectiveEnum.Extroversion:
                        result.EI += perspective.Direction;
                        if (perspective.Selection >= PerspectiveEnum.ConfidencePoint)
                            result.Extroversion++;
                        else
                            result.Introversion++;
                        break;

                    case PerspectiveEnum.Sensing:
                        result.SN += perspective.Direction;
                        if (perspective.Selection <= PerspectiveEnum.ConfidencePoint)
                            result.Sensing++;
                        else
                            result.Intuition++;
                        break;

                    case PerspectiveEnum.Intuition:
                        result.SN += perspective.Direction;
                        if (perspective.Selection >= PerspectiveEnum.ConfidencePoint)
                            result.Intuition++;
                        else
                            result.Sensing++;
                        break;

                    case PerspectiveEnum.Thinking:
                        result.TF += perspective.Direction;
                        if (perspective.Selection <= PerspectiveEnum.ConfidencePoint)
                            result.Thinking++;
                        else
                            result.Feeling++;
                        break;

                    case PerspectiveEnum.Feeling:
                        result.TF += perspective.Direction;
                        if (perspective.Selection >= PerspectiveEnum.ConfidencePoint)
                            result.Feeling++;
                        else
                            result.Thinking++;
                        break;

                    case PerspectiveEnum.Judging:
                        result.JP += perspective.Direction;
                        if (perspective.Selection <= PerspectiveEnum.ConfidencePoint)
                            result.Judging++;
                        else
                            result.Perceiving++;
                        break;

                    case PerspectiveEnum.Perceiving:
                        result.JP += perspective.Direction;
                        if (perspective.Selection >= PerspectiveEnum.ConfidencePoint)
                            result.Perceiving++;
                        else
                            result.Judging++;
                        break;

                }
            }

            if (result.EI > 0)
            {
                result.PerspectiveType += PerspectiveEnum.Introversion;
            }
            else
            {
                result.PerspectiveType += PerspectiveEnum.Extroversion;
            }

            if (result.SN > 0)
            {
                result.PerspectiveType += PerspectiveEnum.Intuition;
            }
            else
            {
                result.PerspectiveType += PerspectiveEnum.Sensing;
            }

            if (result.TF > 0)
            {
                result.PerspectiveType += PerspectiveEnum.Feeling;
            }
            else
            {
                result.PerspectiveType += PerspectiveEnum.Thinking;
            }

            if (result.JP > 0)
            {
                result.PerspectiveType += PerspectiveEnum.Perceiving;
            }
            else
            {
                result.PerspectiveType += PerspectiveEnum.Judging;
            }

            return result;
        }
    }
}